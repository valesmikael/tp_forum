<?php
      // Constante clef de hachage
    define( 'SALT', '8e039b5b5e6988803a04dd9035355b50401df12e4ed047c852e1308851008268');
    
    // Constante BDD
    define( 'DB_HOST', 'localhost' );
    define( 'DB_USER', 'root' );
    define( 'DB_PASS', '' );
    define( 'DB_NAME', 'forum' );

    $mysqli = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

    // Vérification qu'une session est ouverte (sécurisation URL)
    function shouldBeLogged() {
        if( empty( $_SESSION[ 'user' ] ) ) {
            header( 'Location: index.php?page=connexion&error_code=4' );
            die();
        }
    }

    function isLogged() {
        if( empty( $_SESSION[ 'user' ] ) ) {
            return false;
        }
        else {
            return true;
        }
    }
