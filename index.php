<?php 

    //Inclusion du fichier config.php
    require_once 'config.php';

    //Ouverture d'une session
    session_start();

    // Gestions des process
    if( !empty( $_GET['process'] ) ){

        include 'process/' . $_GET['process'] . '.php';
        die();
    }


    // Gestions des pages
    $page = 'connexion';

    // Vérification URL
    if( !empty( $_GET['page'] ) ){

        $page = $_GET['page'];
    }

    // Boucle qui renvois la page demandée et change le title de l'include header

    switch($page){

        case 'connexion' : 
            $title = 'Page de Connexion';
            break;

        case 'inscription':
            $title = 'Page d\'inscription';
            break;

        case 'acceuil':
            shouldBeLogged();
            $title = 'Page d\'acceuil';
            break;

        case 'sujet':
            shouldBeLogged();
            $title = 'Page des sujets';
            break;

        case 'post':
            shouldBeLogged();
            $title = 'Page des post';
            break;

        case 'deconnexion':
            shouldBeLogged();
            $title = 'Page de déconnexion';
            break;
       
    }


    /*----------------------- includes --------------------*/
    
    include_once 'partials/header.php';
    include_once 'partials/nav.php';
    include_once 'pages/' . $page . '.php';
    include_once 'partials/footer.php';
    