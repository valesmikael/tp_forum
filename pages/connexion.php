

<?php 
    $error = [
        1 => 'Tous les champs doivent être remplis',
        2 => 'Utilisateur inconnu',
        3 => 'mot de passe incorrect',
        4 => 'Votre session a expiré, veuillez vous reconnecter'
    ];
    $success = [
        1 => 'Bravo ! vous êtes maintenant inscrit sur ce forum'
    ];

?>

<form action="index.php?process=connexion" method="POST" class="form-group">
    <label for="connexion">Connexion.</label>
    <input type="text" name="username" placeholder='Saisissez votre pseudo' class="form-control">
    <input type="password" name="password" placeholder='Saisissez votre mot de passe' class="form-control">

    <button type="submit" class="btn btn-primary">OK</button>



<?php 

    if( !empty($_GET['error_code']) ){
        
        $error_code = $_GET['error_code'];

        if( !empty($error[$error_code]) ){
            echo '<p class="alert alert-danger">' . $error[$error_code] . '</p>';
        }
        else {
            echo '<p> Erreur 404 </p>';
        }
    }
    if( !empty($_GET['success_code']) ){
        
        $success_code = $_GET['success_code'];

        if( !empty($success[$success_code]) ){
            echo '<p class="alert alert-success">' . $success[$success_code] . '</p>';
        }
        /*else {
            echo '<p> Erreur 404 </p>';
        }*/
    }
    ?>
    </form>