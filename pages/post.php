

<?php include_once 'process/post.php'; ?>

<?php $error = [
        5 => 'Votre message n\'a pu être envoyé, veuillez remplir tous les champs svp.' ,
];

?>

<h1>Post</h1>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th>Titres</th>
            <th>Messages</th>
            <th>Dates</th>
        </tr>
    <thead class="thead-dark">
        <?php foreach( $posts as $post ): ?>
        <tr>
            <td><?php echo $post[ 'title' ]; ?></td>
            <td><?php echo $post[ 'message' ]; ?></td>
            <td><?php echo $post[ 'date' ]; ?></td>
        </tr>
        <?php endforeach; ?>
    
</table>
<h3>Envoyer un message :</h3>
<form method="post" action="index.php?process=nouveau_post" class="form-group">
<input type="hidden" value="<?php echo $_GET['id'] ?>"  name="page" class="form-control">
        <label for="titre">
        Saisissez votre titre!
        </label><br />

    <input type="text" name="titre" placehoder="Votre titre" class="form-control">
   
        <label for="message">
        Saisissez votre message!
        </label><br />
                
        <textarea name="message" id="message" rows="5" cols="70" ></textarea>       
  

    <button type="submit">Envoyer</button>

<?php 

    if( !empty($_GET['error_code']) ){
        
        $error_code = $_GET['error_code'];

        if( !empty($error[$error_code]) ){
            echo '<p class="alert alert-danger">' . $error[$error_code] . '</p>';
        }
        else {
            echo '<p> Erreur 404 </p>';
        }
    }   
?>
</form>
