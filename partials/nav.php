<nav  class="navbar navbar-dark bg-dark">
    <ul class="nav justify-content-center">
        <?php if( !isLogged() ){ ?>
        <li class="nav-item">
            <a href="index.php?page=connexion" class="nav-link">Connexion</a>
        </li>
        <li class="nav-item">
            <a href="index.php?page=inscription" class="nav-link">Inscription</a>
        </li>
        <?php }else { ?>
        <li class="nav-item">
            <a href="index.php?page=acceuil" class="nav-link">Acceuil</a>
        </li>
        <li class="nav-item">
            <a href="index.php?process=deconnexion" class="nav-link">Deconnexion</a>
        </li>
        <?php } ?>
    </ul>
</nav>