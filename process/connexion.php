<?php

    if( empty($_POST['username']) || empty($_POST['password']) ){
        header( 'Location: index.php?page=connexion&error_code=1' );
        die();
    }

    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = 'SELECT * FROM users
        WHERE username = ?';
    
    //Préparation de la requete SQL
    $stmt = mysqli_prepare($mysqli, $sql);
    
    // Insertion des paramètres utilisateur dans la requete
    mysqli_stmt_bind_param($stmt, 's', $username);

    mysqli_stmt_execute( $stmt );
    $result = mysqli_stmt_get_result( $stmt );
    $result = mysqli_fetch_assoc( $result );
    var_dump($result);
    // Fermeture de la commande
    mysqli_stmt_close( $stmt );

    
    if( $result == false ){
        header( 'Location: index.php?page=connexion&error_code=2' );
        die();
    }   
    else {
        $password_hash = hash_hmac('sha256', $password , SALT);

        if( $password_hash === $result['password'] ){
            $_SESSION['user'] = $result;
            header( 'Location: index.php?page=acceuil' );
        }
        else{
            header( 'Location: index.php?page=connexion&error_code=3' );
            die();
        }
    }


    