<?php 

    if( empty($_POST['username']) || empty($_POST['password']) ){
        header( 'Location: index.php?page=inscription&error_code=1' );
        die();
    }
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = 'SELECT username FROM users
        WHERE username = ?';
    
    //Préparation de la requete SQL
    $stmt = mysqli_prepare($mysqli, $sql);
    
    // Insertion des paramètres
    mysqli_stmt_bind_param($stmt, 's', $username);

    mysqli_stmt_execute( $stmt );
    $result = mysqli_stmt_get_result( $stmt );
    $result = mysqli_fetch_assoc( $result );

    // Fermeture de la commande
    mysqli_stmt_close( $stmt );

    if( $result ){
        header( 'Location: index.php?page=inscription&error_code=2' );
    }
    else {

        $password_hash = hash_hmac('sha256', $password , SALT);

        $sql = 'INSERT INTO `forum`.`users` (`username`, `password`) VALUES (?, ?)';
        
        //Préparation de la requete SQL
        $stmt = mysqli_prepare($mysqli, $sql);
        
        // Insertion des paramètres
        mysqli_stmt_bind_param($stmt, 'ss', $username, $password_hash);
        
        //creation des colonne
        mysqli_stmt_execute( $stmt );
        $result =  mysqli_stmt_affected_rows( $stmt );
        
        // Fermeture de la commande
        mysqli_stmt_close( $stmt );

        if( $result > 0){
            header( 'Location: index.php?page=connexion&success_code=1' );
            die();
        }
    }