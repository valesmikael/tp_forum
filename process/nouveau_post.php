<?php

$titre = $_POST['titre'];
$message = $_POST['message'];
$page = $_POST['page'];

if( empty($titre)|| empty($message)){
    header( 'Location: index.php?page=post&id='.$page.'&error_code=5');
    die();
}
    
 
$sql = 'INSERT INTO `forum`.`posts`(`title`,`message`,`sujets_id`) VALUES (?, ?, ?)';
        
    
    //Préparation de la requete SQL
    $stmt = mysqli_prepare($mysqli, $sql);
    
    // Insertion des paramètres utilisateur dans la requete
    mysqli_stmt_bind_param($stmt, 'ssi', $titre, $message, $page);

    mysqli_stmt_execute( $stmt );
    // Fermeture de la commande
    mysqli_stmt_close( $stmt );
    
    $result_id=mysqli_insert_id($mysqli);


if ($result_id == false){
    header(  'Location: index.php?page=post&id='.$page.'&error_code=5');
    die();
}else{
    header( 'Location: index.php?page=post&id='.$page );
    die();
}
    

    

?>